<?php
/**
 * autoSubmit Plugin for LimeSurvey
 * Allow to set survey auto submitting for some question.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2014-2020 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 0.2.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
class autoSubmit extends PluginBase {
  protected $storage = 'DbStorage';
  static protected $description = 'Allow to set survey auto submitting for question with only one choice.';
  static protected $name = 'Auto submit 1.0';

  protected $settings = array(
    'activeall'=>array(
      'type'=>'boolean',
      'label'=>'Activate auto submit by default on all survey',
      'default'=>0,
    ),
  );

  public function init() {
    $this->subscribe('beforeSurveyPage');
    $this->subscribe('beforeSurveySettings');
    $this->subscribe('newSurveySettings');
  }

  public function beforeSurveyPage() {
    if (!$this->getEvent()) {
        throw new CHttpException(403);
    }
    $oEvent = $this->event;
    $sSurveyParam=$this->get('active', 'Survey', $oEvent->get('surveyId'),'D');

    if($sSurveyParam=='Y' || ($sSurveyParam=='D' && $this->get('activeall')))
    {
        App()->clientScript->registerScript('bAutoMove',"bAutoMove=true;",CClientScript::POS_BEGIN);
        $jsUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/autosubmit.js');
        App()->clientScript->registerScriptFile($jsUrl,CClientScript::POS_END);
    }
  }
  /**
   * This event is fired by the administration panel to gather extra settings
   * available for a survey.
   * The plugin should return setting meta data.
   * @param PluginEvent $event
   */
  public function beforeSurveySettings() {
    if (!$this->getEvent()) {
        throw new CHttpException(403);
    }
    $oEvent = $this->event;
    $sCssFix='#s2id_plugin_autoSubmit_active{min-width:10em}';
    App()->clientScript->registerCss('autoSubmitFix',$sCssFix);
    $oEvent->set("surveysettings.{$this->id}", array(
      'name' => get_class($this),
      'settings' => array(
        'active'=>array(
           'style'=>'min-width:10em',
          'type'=>'select',
          'label'=>'Activate the auto submit',
          'options'=>array(
            'D'=>sprintf("Leave default (%s)",$this->get('activeall') ? "On" : "Off"),
            'Y'=>"Force on",
            'N'=>"Force off",
          ),
          'current' => $this->get('active', 'Survey', $oEvent->get('survey'),'D'),
          'help' => sprintf("Actual default otion is : %s",$this->get('activeall') ? "On" : "Off"),
        )
      )
    ));
  }

  public function newSurveySettings()
  {
    if (!$this->getEvent()) {
        throw new CHttpException(403);
    }
      $oEvent = $this->event;
      foreach ($oEvent->get('settings') as $name => $value)
      {
          /* In order use survey setting, if not set, use global, if not set use default */
          $default=$oEvent->get($name,null,null,isset($this->settings[$name]['default'])?$this->settings[$name]['default']:null);
          $this->set($name, $value, 'Survey', $oEvent->get('survey'),$default);
      }
  }
}
